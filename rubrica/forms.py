from django import forms

from .models import Contact


class ContactForm(forms.ModelForm):
    change_reason = forms.CharField(widget=forms.Textarea)


    class Meta:
        model = Contact
        fields = '__all__'
#        fields = ('name', 'number', 'change_reason', )
