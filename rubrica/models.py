from django.db import models
from simple_history.models import HistoricalRecords


class Contact(models.Model):
    name = models.CharField(max_length=200)
    number = models.CharField(max_length=30)
    history = HistoricalRecords()

    def __str__(self):
        return self.name
