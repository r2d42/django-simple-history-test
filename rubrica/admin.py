from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from .models import Contact

class ContactHistoryAdmin(SimpleHistoryAdmin):
    history_list_display = ["number"]

admin.site.register(Contact, ContactHistoryAdmin)

