from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('new/', views.contact_new, name='contact_new'),
    path('edit/<int:pk>/', views.contact_edit, name='contact_edit'),
    path('history/<int:pk>/', views.contact_history, name='contact_history'),
]
