from django.shortcuts import render, redirect, get_object_or_404

from .models import Contact
from .forms import ContactForm


def index(request):
    all_contacts = Contact.objects.all()
    context = {'all_contacts': all_contacts}
    return render(request, 'rubrica/index.html', context)

def contact_new(request):
    if request.method == "POST":
       form = ContactForm(request.POST)
       if form.is_valid():
        form.save()
        return redirect('index')
    else:
        form = ContactForm()
    return render(request, 'rubrica/contact_edit.html', {'form': form})

def contact_edit(request, pk):
    contact = get_object_or_404(Contact, pk=pk)
    if request.method == "POST":
       form = ContactForm(request.POST, instance=contact)
       if form.is_valid():
        contact = form.save(commit=False)
        contact.name = form.cleaned_data['name']
        contact.number = form.cleaned_data['number']
        contact.changeReason = form.cleaned_data['change_reason']
        contact.save()
        return redirect('index')
       else:
           print('not valid form')
    else:
        form = ContactForm(instance=contact)

    return render(request, 'rubrica/contact_edit.html', {'form': form, 'contact':contact })

def contact_history(request, pk):
    contact = get_object_or_404(Contact, pk=pk)
    contact_history = contact.history.all()
    context = {'contact':contact, 'contact_history': contact_history}

    return render(request, 'rubrica/contact_history.html', context)
