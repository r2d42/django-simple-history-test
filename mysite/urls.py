from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('rubrica.urls')),
    path('admin/', admin.site.urls),
]
